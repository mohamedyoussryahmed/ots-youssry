<center> <h1>On The Shelve (OTS) - Visual Studio README </h1> </center>

<h2> Table of contents </h2>

- [Description](#description)
- [Visual Studio in MedSoft](#visual-studio-in-medsoft)
- [Visual Studio Version](#visual-studio-version)

---

## Description

Microsoft Visual Studio is an integrated development environment from Microsoft. It is used to develop computer programs, as well as websites, web apps, web services and mobile apps.

## Visual Studio in MedSoft

We use Visual Studio as IDE to develop and integrate all our software components and programs.

## Visual Studio Version

We will use Visual Studio 2019 Community edition


