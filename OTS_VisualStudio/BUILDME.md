<center> <h1>On The Shelve (OTS) - Visual Studio Installation </h1> </center>

<h2> Table of contents </h2>

- [Installation Steps](#installation-steps)
  - [Download Visual Studio](#download-visual-studio)
- [FAQ](#faq)
- [BUILDME info](#buildme-info)

---

## Installation Steps

### Download Visual Studio

- Go to the [old version download page](https://visualstudio.microsoft.com/vs/older-downloads/) from the Microsoft Visual Studio website.
- Scroll down to find the title “Still Want an Older Version?”
- Select 2019 from the menu and click Download
- You need to login using Microsoft account, so if you have no e-mail you should create an email on Micosoft.
-  Scroll down and find the `Visual Studio Community 2019 (version 16.11)` and click download.

![Visual Studio Community 2019 (version 16.11) Download](./buildme-assets/VisualStudioDownload.png)


- If the item displayed is not Visual Studio 2019 , type in the search box visual studio 2019 and select the community edition and click download
- The online installer for VS 2019 will be downloaded, double click and run the installer
- Follow the steps in the installer until you reach the window showing the components that need to be selected. Make sure to select (at least) “Desktop development with C++”. And if you need to change the installation path, this is the time as well.
- Then, click install. This will download and install the selected components.
- In case of Desktop development with C++ is selected alone, the download size will be around 2GB.

---

## FAQ

This FAQ will be updated if any future errors appeared on this installation.


---

## BUILDME info

- This BUILDME file was created at `13/April/2022` by:
  - Eng.Mohamed Youssry `mohamedyoussryahmed@gmail.com`
  - Eng.Shaimaa salem `sgamal@tmcmf.com`
  - Eng.Mohamed El-Asklany *(superviser)* `malaskalny@tmcmf.com`