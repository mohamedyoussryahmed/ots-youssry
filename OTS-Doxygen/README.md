<center> <h1>On The Shelve (OTS) - Doxygen README </h1> </center>

<h2> Table of contents </h2>

- [Description](#description)
- [Doxygen in MedSoft](#doxygen-in-medsoft)

---

## Description

Doxygen is a documentation generator and static analysis tool for software source trees. When used as a documentation generator, Doxygen extracts information from specially-formatted comments within the code. When used for analysis, Doxygen uses its parse tree to generate diagrams and charts of the code structure.

## Doxygen in MedSoft

We use Doxygen in automatic documentation and diagrams generation from the code.

Also, Doxygen is configured in the main cmake and each library’s cmake to be generated upon each
build of the respective library (given the cmake “generate docs” is checked).

