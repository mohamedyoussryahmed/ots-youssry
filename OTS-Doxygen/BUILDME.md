<center> <h1>On The Shelve (OTS) - Doxygen Installation </h1> </center>

<h2> Table of contents </h2>

- [Building Steps](#building-steps)
  - [Step 1: System Requirements](#step-1-system-requirements)
  - [Step 2: Download Doxygen](#step-2-download-doxygen)
- [FAQ](#faq)
- [BUILDME info](#buildme-info)

---

## Building Steps

We dpend on the [Doxygen installation official documentation](https://www.doxygen.nl/manual/install.html#install_src_windows) to install doxygen

### Step 1: System Requirements

- Download Flex and Bison [Flex and Bison Zip file Download](https://sourceforge.net/projects/winflexbison/)
  - After downloding this zip file, unzipped it in any directory you want and rename this directory with any name for example as `flex`. so the path of your unzipped file should be something like `G:\MedSoft\Software\flex`
  - now inside the `flex` directory rename `win_flex.exe` to `flex.exe` and `win_bison.exe` to `bison.exe` 
  - Add this directory path to the system environment vairables. *see the image below*

![Doxygen Flex](./buildme-assets/DoxFlex.png)

- Make sure that you have CMake installed and working on your system.
- Make sure that you have python installed and added to your system environment.

### Step 2: Download Doxygen

- open `cmd` and navigate to your preffered directory.
- write in your `cmd` this command to clone the doxygen source code: `git clone https://github.com/doxygen/doxygen.git`
- We will assume that the path of the doxygen source directory is `G:\MedSoft\OTS\Doxygen` to use it as a reference path for this documentation.
- create a `build` directory inside the doxygen directory. the build directory would have the path `G:\MedSoft\OTS\Doxygen\build`
- Open `CMake GUI` and put the doxygen source path and the doxygen build path as shown in the image below.

![Doxygen CMake](./buildme-assets/DoxCMake.png)

- Click `configure` button and choose your generator as x64 visual stuido 2016 19.
- Make a directory at the same level of the doxygen source directory called `doxygen_install`. its path would be for example `G:\MedSoft\OTS\doxygen_install`
- Change the `CMAKE_INSTALL_PREFIX` in the CMake GUI to other directory you created by yourself and its path is for example `G:\MedSoft\OTS\doxygen_install`.
- Click on the `configure` button again then click on `generate` button, then click on the `open project` that will open the `blender.sln` project from the build directory in a visual studio 2019.
- Make sure that you are on the `debug` mode.
- Right click on the `ALL_BUILD` and choose `build`.
- After building is finished go to `INSTALL` and right click on it and choose `build`.
- Change the mode to the `release` mode and repeat the steps for the `ALL_BUILD` and `INSTALL`.
  
---

## FAQ

This FAQ will be updated if any future errors appeared on this installation.


---

## BUILDME info

- This BUILDME file was created at `10/April/2022` by:
  - Eng.Mohamed Youssry `mohamedyoussryahmed@gmail.com`
  - Eng.Shaimaa salem `sgamal@tmcmf.com`
  - Eng.Mohamed El-Asklany *(superviser)* `malaskalny@tmcmf.com`